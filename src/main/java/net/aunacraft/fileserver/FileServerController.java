package net.aunacraft.fileserver;

import net.aunacraft.fileserver.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.FileNotFoundException;
import java.io.IOException;

@Controller
public class FileServerController {

    private final StorageService storageService;

    @Autowired
    public FileServerController(StorageService storageService) {
        this.storageService = storageService;
    }

    @PostMapping("/upload/**")
    public ResponseEntity<Resource> fileUpload(@RequestParam("fileName") String fileName, @RequestParam("file") MultipartFile file) {
        try {
            this.storageService.saveFile(fileName, file.getInputStream());
            return ResponseEntity.ok().build();
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/download/**")
    public ResponseEntity<Resource> downloadFile(@RequestParam("fileName") String fileName) {
        try {
                FileSystemResource resource = new FileSystemResource(this.storageService.loadFile(fileName));
                String contentType = "application/octet-stream";
                return ResponseEntity.ok()
                        .contentType(MediaType.parseMediaType(contentType))
                        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                        .body(resource);
        } catch (FileNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

}
