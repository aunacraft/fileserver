package net.aunacraft.fileserver.storage;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class FileSystemStorageService implements StorageService {

    private static Logger LOGGER = LoggerFactory.getLogger("FileServer");
    private final Path rootLocation;

    @Autowired
    public FileSystemStorageService(StorageProperties properties) {
        File rootDir = new File(properties.getLocation());
        if(!rootDir.exists())
            rootDir.mkdir();
        this.rootLocation = rootDir.toPath();

        try {
            Files.createDirectories(rootLocation);
        }
        catch (IOException e) {
            throw new RuntimeException("Could not initialize storage", e);
        }
    }

    @Override
    public void saveFile(String fileName, InputStream inputStream) throws IOException {
        LOGGER.info("Saving file: " + fileName);
        byte[] buffer = new byte[inputStream.available()];
        inputStream.read(buffer);
        File targetFile = new File(rootLocation.toFile(), fileName);
        if(targetFile.exists())
            targetFile.delete();

        OutputStream outStream = new FileOutputStream(targetFile);
        outStream.write(buffer);
        outStream.close();
    }

    @Override
    public File loadFile(String fileName) throws FileNotFoundException {
        LOGGER.info("Loading file: " + fileName);
        File file = new File(rootLocation.toFile(), fileName);
        if(!file.exists())
            throw new FileNotFoundException();
        return file;
    }
}
