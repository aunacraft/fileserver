package net.aunacraft.fileserver.storage;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.stream.Stream;

public interface StorageService {

    void saveFile(String fileName, InputStream inputStream) throws IOException;

    File loadFile(String fileName) throws FileNotFoundException;

}
